const neo4j = require('neo4j-driver');
const { MongoClient, ObjectID } = require('mongodb');

const driver = neo4j.driver(
	'neo4j://localhost',
	neo4j.auth.basic('neo4j', 'firefox2'),
);

const session = driver.session({
	database: 'neo4j',
	defaultAccessMode: neo4j.session.WRITE,
});

async function createGraph() {
	await driver.verifyConnectivity();
	
	const url = 'mongodb://localhost:27017';
	const dbName = 'isis_ng';
	const client = await MongoClient.connect(url);
	const db = client.db(dbName);
	
	const transaction = session.beginTransaction();
	
	transaction.run('match ()-[p]->() delete p');
	transaction.run('match (n) delete n');
	
	const connectables = await db.collection('networkconnectables').find({
		project: ObjectID('5ac26587c4f2410019f74971'),
		deletedAt: null,
	}).toArray();
	
	console.log('Total: ', connectables.length);
	for (let i = 0; i < connectables.length; ++i) {
		const connectable = connectables[i];
		
		if (connectable.isDrop) {
			const property = await db.collection('boxes').findOne({ drop: connectable.parent });
			const client = await db.collection('clients').findOne({ _id: property.client });
			
			connectable.name = client && client.code || property.address;
		}
		
		transaction.run(`CREATE (connectable:NetworkConnectable:${ connectable.kind }${ connectable.isDrop ? ':CLIENT' : '' } { name: $name, parent: $parent, id: $_id })`, {
			kind: connectable.kind,
			name: connectable.name || '',
			_id: connectable._id.toString(),
			parent: connectable.parent.toString(),
		});
	}
	
	const connector_map = new Map();
	const connectors = await db.collection('networkconnectors').find({
		project: ObjectID('5ac26587c4f2410019f74971'),
		deletedAt: null,
	}).toArray();
	console.log('Total: ', connectors.length);
	for (const connector of connectors) {
		if (connector.kind === 'DIO') {
			for (let i = 0; i < connector.connectables.input.length; ++i) {
				transaction.run('CREATE (connector:NetworkConnector { name: $name, parent: $parent, id: $_id })', {
					kind: connector.kind,
					name: connector.name + ' Porta ' + (i + 1),
					_id: connector._id.toString() + i,
					parent: connector.parent.toString(),
				});
			}
		} else {
			transaction.run('CREATE (connector:NetworkConnector { name: $name, parent: $parent, id: $_id })', {
				kind: connector.kind,
				name: connector.name || '',
				_id: connector._id.toString(),
				parent: connector.parent.toString(),
			});
		}
		
		connector_map.set(connector._id.toString(), connector);
	}
	
	console.log('criando as conexões');
	const files = require('fs').readdirSync(require('path').join(__dirname, 'connection'));
	for (const file of files) {
		const connections = JSON.parse(require('fs').readFileSync('/home/christian/Desktop/connections/' + file).toString());
		for (const connection of connections) {
			let [id1, id2] = connection.split('=>');
			
			const connector = connector_map.has(id1) ? connector_map.get(id1) : connector_map.get(id2);
			if (connector.kind === 'DIO') {
				if (connector_map.has(id1)) {
					const index = connector.connectables.input.includes(id2) ? connector.connectables.input.indexOf(id2) : connector.connectables.output.indexOf(id2);
					id1 = id1 + index;
				} else {
					const index = connector.connectables.input.includes(id1) ? connector.connectables.input.indexOf(id1) : connector.connectables.output.indexOf(id1);
					id2 = id2 + index;
				}
				
				transaction.run(`
					MATCH (a), (b)
					WHERE a.id = "${ id1 }" AND b.id = "${ id2 }"
					CREATE (a)-[r:SERVE { attenuation: 0 }]->(b)
					RETURN type(r)
				`);
			} else {
				transaction.run(`
					MATCH (a), (b)
					WHERE a.id = "${ id1 }" AND b.id = "${ id2 }"
					CREATE (a)-[r:SERVE { attenuation: 0 }]->(b)
					RETURN type(r)
				`);
			}
		}
	}
	
	await transaction.commit();
	
	console.log('fim');
}

(async () => {
	console.time('Buscando da PON pra baixo');
	const result = await session.run('match result=(a {id: "5ccddeacd7cefb49b0bb9242"})<-[r:SERVE*]-(b) where not ()-[:SERVE]->(b) return result;');
	// const result = await session.run('match result=(a {id: "5c582b198f754d00168c1b74"})-[r:SERVE*]->(b) return result;');
	console.timeEnd('Buscando da PON pra baixo');
	debugger;
})();
